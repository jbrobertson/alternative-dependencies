import collections

import networkx
import z3
from typing import Optional, Set

from .dependencies import Dependencies
from .z3_util import get_solutions_with_cache, get_all_solutions


def minimise_loops(
        dependencies: Dependencies,
        weights: Optional[dict] = None,
        use_cache=False):
    if weights is None:
        weights = collections.defaultdict(lambda: 1)
    solver = _Solver(dependencies)
    return solver.optimise(weights, use_cache)


class _Solver:
    def __init__(self, all_dependencies: Dependencies):
        self._opt = z3.Optimize()
        self._indexes = {}
        self._index_rev = {}
        self._add_dependencies(self._opt, all_dependencies.get_dependencies())

    def _get_index(self, var: str) -> int:
        index = self._indexes.get(var)
        if index is not None:
            return index
        else:
            index = len(self._indexes)
            self._indexes[var] = index
            self._index_rev[index] = var
            return index

    def _get_dep_var(self, k, v) -> str:
        k_index = self._get_index(k)
        v_index = self._get_index(v)
        return f"d_{k_index}_{v_index}"

    def _add_dependencies(self, solver: z3.Optimize, all_dependencies):
        for target, dependencies in all_dependencies:
            conjunction = []
            for k in dependencies:
                if type(k) == str:
                    clause = z3.Bool(self._get_dep_var(target, k))
                else:
                    clause = z3.And([
                        z3.Bool(self._get_dep_var(target, kk))
                        for kk in k
                    ])
                conjunction.append(clause)
            if len(conjunction) == 1:
                solver.add(conjunction[0])
            else:
                solver.add(z3.Or(conjunction))

    def _translate(self, solution):
        translated_solution = []
        for dep in solution:
            a, b = dep[2:].split("_")
            da = self._index_rev[int(a)]
            db = self._index_rev[int(b)]
            translated_solution.append((da, db))
        return translated_solution

    def _make_graph(self, solution):
        digraph = networkx.DiGraph()
        for a, b in self._translate(solution):
            digraph.add_edge(a, b)
        return digraph

    def _get_score(self, weights: dict, solution: Set[str]):
        digraph = self._make_graph(solution)
        cycles = list(networkx.algorithms.simple_cycles(digraph))
        score = sum(
            weights[node] for cycle in cycles
            for node in cycle
        )
        return score

    def optimise(self, weights, use_cache=False):
        best, best_score = None, None
        if use_cache:
            solutions = get_solutions_with_cache(self._opt)
        else:
            solutions = get_all_solutions(self._opt)

        for solution in solutions:
            score = self._get_score(weights, solution)
            if score == 0:
                best = solution
                break
            elif best_score is None or score < best_score:
                best_score = score
                best = solution

        if best is None:
            raise Exception("No solution found")

        return self._translate(best)
