import z3


def get_solutions_with_cache(solver: z3.Optimize):
    cache = set()
    for solution in get_all_solutions(solver):
        for c in cache:
            if solution.issuperset(c):
                break
        else:
            cache.add(solution)
            yield solution


def get_all_solutions(solver: z3.Optimize):
    while solver.check() == z3.sat:
        model = solver.model()

        dependencies = frozenset(
            k.name() for k in model
            if bool(model[k]) is True
        )

        yield dependencies

        negate_this = z3.Not(z3.And([
            z3.Bool(k.name()) == bool(model[k]) for k in model
        ]))
        solver.add(negate_this)
