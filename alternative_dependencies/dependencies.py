from typing import Union, List, Tuple


class Dependencies:
    def __init__(self):
        self._dependencies = []  # type: List[Tuple[str, List[Union[str, List[str]]]]]

    def add_dependency_one_of(self, target: str,
                              *dependencies: Union[str, List[str]]):
        self._dependencies.append((target, list(dependencies)))

    def get_dependencies(self) -> List[Tuple[str, List[Union[str, List[str]]]]]:
        return self._dependencies
