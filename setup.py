#!/usr/bin/env python

import setuptools


setuptools.setup(
    name='alternative-dependencies',
    version='0.1',
    description='Exploring the scheduling of tasks shared among groups',
    author='JB Robertson',
    author_email='jbr@sdf.org',
    url='https://www.gitlab.com/jbrobertson/alternative-dependencies',
    packages=['alternative_dependencies'],
    python_requires='>=3.8',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "networkx==2.6.2",
        "z3-solver==4.8.9.0",
    ]
)
