import pytest
import z3

from alternative_dependencies import z3_util


@pytest.fixture
def solver():
    s = z3.Optimize()
    s.add(z3.Or(z3.Bool("A"), z3.Bool("B")))
    return s


def test_get_all_solutions(solver):
    solutions = set(z3_util.get_all_solutions(solver))
    print(solutions)
    assert solutions == {
        frozenset("A"),
        frozenset("B"),
        frozenset({"A", "B"}),
    }


def test_get_solutions_with_cache(solver):
    solutions = set(z3_util.get_solutions_with_cache(solver))
    print(solutions)
    assert solutions == {
        frozenset("A"),
        frozenset("B")
    }
