import mock
import networkx
import pytest

from alternative_dependencies import dependencies as _dependencies
from alternative_dependencies import optimiser


@pytest.fixture
def dependencies():
    s = _dependencies.Dependencies()
    s.add_dependency_one_of("A", "B", "C")
    s.add_dependency_one_of("C", "D")
    s.add_dependency_one_of("D", "E")
    s.add_dependency_one_of("E", "C", "F", "A")
    return s


def test_optimise(dependencies):
    best = optimiser.minimise_loops(dependencies)
    assert set(best) == {('D', 'E'), ('C', 'D'), ('E', 'F'), ('A', 'B')}


@pytest.mark.parametrize(
    "solution, expected_score",
    [
        ([("A", "B")], 0),
        ([("C", "D"), ("D", "E"), ("E", "C")], 6),
        ([("E", "A"), ("C", "D"), ("D", "E"), ("E", "C"), ("A", "C")], 13)
    ]
)
def test_get_score_from_solver(solution, expected_score, dependencies):
    weights = {
        "A": 1, "B": 1, "C": 2, "D": 1, "E": 3
    }
    solver = optimiser._Solver(dependencies)
    solution = [
        solver._get_dep_var(*sol) for sol in solution
    ]
    assert expected_score == solver._get_score(weights=weights, solution=solution)


def test_translate(dependencies):
    solver = optimiser._Solver(dependencies)
    s = solver._get_dep_var("A", "Z")
    assert [("A", "Z")] == solver._translate([s])


def test_get_score(dependencies):
    solver = optimiser._Solver(dependencies)
    graph = networkx.DiGraph()
    graph.add_edge("A", "B")
    graph.add_edge("B", "C")
    graph.add_edge("C", "A")
    weights = dict(A=1, B=2, C=3)

    with mock.patch("alternative_dependencies.optimiser._Solver._make_graph",
                    return_value=graph):
        score = solver._get_score(weights, mock.MagicMock())
    assert 6 == score
