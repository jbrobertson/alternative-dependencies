from alternative_dependencies import dependencies


def test_dependencies():
    dep = dependencies.Dependencies()
    dep.add_dependency_one_of("A", "B", "C")
    dep.add_dependency_one_of("D", ["E", "F"], "G")
    assert ("D", [["E", "F"], "G"]) in dep.get_dependencies()
    assert ("A", ["B", "C"]) in dep.get_dependencies()
